const Sequelize = require('sequelize');
const config = require("../../config/environment");
const sequelize = new Sequelize(config.uri);

const User = sequelize.define('user', {
  name: {
    type: sequelize.Sequelize.STRING
  },
  gender: {
    type: sequelize.Sequelize.STRING
  },
  email: {
    type: sequelize.Sequelize.STRING
  },
  password: {
    type: sequelize.Sequelize.STRING
  }
});

module.exports = {
  add: (data) => {
    return User.sync().then(() => {
      return User.create(data);
    });
  },

  findOne: (condition, data) => {
    return User.sync().then(() => {
      return User.find({
        where: {
          email: condition
        }
      }).then((res, err) => {
        data(res)
      })
    })
  }
}

const router = require('express-promise-router')();
const controller = require('./controller');
const Middleware = require('../middleware');

router.post('/addUser',
  controller.addUser,
  Middleware.sendResponse
);

router.post('/loginUser',
  controller.loginUser,
  Middleware.sendResponse
);

module.exports = router;

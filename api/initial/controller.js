const User = require('./model');
const CryptoJS = require('crypto-js');
const config = require('../../config/environment');

exports.addUser = (req, res, next) => {
  const data = req.body;

  User
  .findOne(data.email, (user) => {
    if(user) {
      req.data = {
        message: 'Email already exist'
      }
      return next();
    }
    data.password = CryptoJS.AES.encrypt(data.password, config.PASSWORD_SECRET_KEY).toString();
    User
    .add(data)
    .then((res) => {
      req.data = res
      return next()
    })
  })

};

exports.loginUser = (req, res, next) => {
  const data = req.body;

  let userData;

  User
  .findOne(data.email, (user) => {
    if(!user) {
      req.data = {
        message: 'User does not exist'
      }
      return next();
    }

    userData = user
    const password = CryptoJS.AES.decrypt(userData.password, config.PASSWORD_SECRET_KEY).toString(CryptoJS.enc.Utf8);
    if(data.password !== password) {
      req.data = {
        message: 'Invalid Email or Password'
      }
      return next();
    }
    req.data =  {
      message: 'success'
    }
    return next();
  })
}

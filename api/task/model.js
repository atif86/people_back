const Sequelize = require('sequelize');
const config = require("../../config/environment");
const sequelize = new Sequelize(config.uri);

const Task = sequelize.define('task', {
  description: {
    type: sequelize.Sequelize.STRING
  },
  user_id: {
    type: sequelize.Sequelize.INTEGER
  },
  date: {
    type: sequelize.Sequelize.STRING,
  },
  start_time: {
    type: sequelize.Sequelize.STRING,
  },
  end_time: {
    type: sequelize.Sequelize.STRING,
  }
});

module.exports = {

  add : (data) => Task.create(data),

  findOne: (id, data) => {
    return Task.sync().then(() => {
      return Task.find({
        where: {
          task_id: id
        }
      }).then((res, err) => {
        data(res);
      })
    })
  },

  findByDate: (date) => {
    return Task.findAll({
      where: {
        date: date
      }
    }).then((res) => {
      return res
    })
  },

  findAll: (id, data) => {
    return Task.sync().then(() => {
      return Task.findAll({ where: { user_id: id }}).then((res, err) => {
        data(res);
      })
    })
  },

  findOneAndUpdate: (id, updateData, data) => {
    return Task.update(updateData, { where: { id: id }}).then((res, err) => {
      data(res);
    })
  },

  getAllTask: (data) => {
    return Task.findAll().then((res, err) => {
      data(res);
    })
  }
}

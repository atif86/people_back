const router = require('express-promise-router')();
const controller = require('./controller');
const Middleware = require('../middleware');

router.post('/addTask',
  controller.addTask,
  Middleware.sendResponse
);

router.post('/editTask',
  controller.editTask,
  Middleware.sendResponse
);

router.post('/getTaskByDate',
  controller.getTaskByDate,
  Middleware.sendResponse
);

module.exports = router;

const Task = require('./model');

exports.addTask = (req, res, next) => {
  const data = req.body;

  Task
  .add(data)
  .then((res) => {
    req.data = res
    return next();
  })
};

exports.editTask = (req, res, next) => {
  const data = req.body;

  Task
  .findOneAndUpdate(data.id, data, (res) => {
    req.data = res;
    return next()
  })
};

exports.getTaskByDate = (req, res, next) => {
  const data = req.body;

  Task
  .findByDate(data.date)
  .then((res) => {
    req.data = res;
    return next()
  })
}

const express = require("express");
const Sequelize = require('sequelize');
const config = require("./config/environment");

const sequelize = new Sequelize(config.uri);

const app = express();
const server = require('http').createServer(app);

require('./config/express')(app);
require('./routes')(app);

server.listen(config.port, () => {
  console.log('People back app listening on port 8080!')
});

module.exports = server;

const initial = require('./api/initial');
const task = require('./api/task');

module.exports = (app) => {
    app.use('/api', initial);
    app.use('/api/task', task);
};

module.exports = {
  postgres: {
    uri: 'postgres://postgres_user:atif@localhost:5432/people_back'
  },

  COOKIE_DOMAIN: 'localhost',
  whitelist: ['localhost:3000']
}

const _ = require('lodash');

const envConfig = require(`./${process.env.NODE_ENV || 'development'}.js`);

const all = {

  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 8080,

  uri: 'postgres://postgres_user:atif@localhost:5432/people_back',

  PASSWORD_SECRET_KEY: 'peopleback@test123'
};

module.exports = _.merge(all, envConfig);
